import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

for (def n : (0..2)) {
    WebUI.selectOptionByValue(findTestObject('Object Repository/PageCURAHealthcare/select_Tokyo'), 
        'Hongkong CURA Healthcare Center', true)

    WebUI.click(findTestObject('Object Repository/PageCURAHealthcare/input_hospital_readmission'))

    if (n == 0) {
        WebUI.click(findTestObject('PageCURAHealthcare/input_Medicare'))
    } else if (n == 1) {
        WebUI.click(findTestObject('PageCURAHealthcare/input_medicaid'))
    } else {
        WebUI.click(findTestObject('PageCURAHealthcare/input_None'))
    }
    
    WebUI.click(findTestObject('Object Repository/PageCURAHealthcare/input_visit_date'))

    WebUI.click(findTestObject('Object Repository/PageCURAHealthcare/td_24'))

    WebUI.setText(findTestObject('Object Repository/PageCURAHealthcare/textarea_comment'), 'No Comment')

    WebUI.click(findTestObject('Object Repository/PageCURAHealthcare/button_Book Appointment'))

    WebUI.navigateToUrl('https://katalon-demo-cura.herokuapp.com/appointment.php#summary')

    WebUI.delay(1)

    WebUI.click(findTestObject('Object Repository/PageCURAHealthcare/a_Go to Homepage'))
}

WebUI.takeScreenshotAsCheckpoint('BookingAppointment')

WebUI.closeBrowser()

