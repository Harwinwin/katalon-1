import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://130.211.243.37:96/login')

WebUI.setText(findTestObject('Object Repository/CMS E-Learn/Page_Login  Elearning CMS/input_email'), 'admin@indocyber.com')

WebUI.setEncryptedText(findTestObject('Object Repository/CMS E-Learn/Page_Login  Elearning CMS/input_password'), 'hUKwJTbofgPU9eVlw/CnDQ==')

WebUI.click(findTestObject('Object Repository/CMS E-Learn/Page_Login  Elearning CMS/inputrememberMe'))

WebUI.click(findTestObject('Object Repository/CMS E-Learn/Page_Login  Elearning CMS/button_Login'))

WebUI.click(findTestObject('Object Repository/CMS E-Learn/Page_Home  Elearning CMS/span_Master Question'))

WebUI.click(findTestObject('Object Repository/CMS E-Learn/Page_Master Quiz  Elearning CMS/other/button_Tambah                              _531150'))

WebUI.click(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/Level/arrow'))

WebUI.setText(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/Level/Textbox'), 'ISO')

WebUI.sendKeys(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/Level/Textbox'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/Module/arrowModule'))

WebUI.setText(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/Module/InputModule'), 'ISO 27001')

WebUI.sendKeys(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/Module/InputModule'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/Chapter/arrowChap'))

WebUI.setText(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/Chapter/InputChap'), 'Materi 2')

WebUI.sendKeys(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/Chapter/InputChap'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/CMS E-Learn/Page_Master Quiz  Elearning CMS/other/textarea_desc'), 'apa ya')

WebUI.setText(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/other/option a'), 'a')

WebUI.setText(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/other/option b'), 'b')

WebUI.setText(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/other/option c'), 'c')

WebUI.setText(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/other/option d'), 'd')

WebUI.click(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/other/radiobtn'))

WebUI.click(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/other/checkbox'))

WebUI.click(findTestObject('CMS E-Learn/Page_Master Quiz  Elearning CMS/other/save'))

WebUI.delay(2)

WebUI.refresh()

